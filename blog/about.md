---
layout: about
title: About
permalink: /about/
---

Dit is de blogpagina van Groep 14 van SpInDP 2016-2017, en onze spin Chubby Checker.
Voor onze leden kunt u kijken op [deze pagina](/spindp14/2017/05/02/teamleden.html).

De website is gemaakt met [Jekyll](https://jekyllrb.com/), en het thema is [Flex](https://github.com/the-development/flex).
