---
cover: cover.jpg
layout: post
title:  "WebApp proof-of-concept"
date:   2017-05-04 18:39:27 +0200
---

Binnen een enkele twee uurtjes snel een Flask WebApp gemaakt die live updates
doet in de browser van een random number generator die op de server (de spin)
loopt.  Uiteindelijk worden de gegevens van de spin hier live weergegeven!

<iframe width="640" height="360" src="{{ site.baseurl }}/assets/proof_of_concept.webm" frameborder="0" allowfullscreen></iframe>
