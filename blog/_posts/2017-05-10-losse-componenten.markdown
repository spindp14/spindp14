---
layout: post
cover: cover.jpg
title:  "Losse componenten"
date:   2017-05-10 15:49:27 +0200
---

Vandaag zijn er meerdere prototypes van componenten klaargemaakt.  Allereerst is
er een prototype-poot:

![Poot]({{ site.baseurl }}/assets/poot.jpg)

We kunnen nu ook tekenen op de touchscreen:

![Touchscreen]({{ site.baseurl }}/assets/touchscreen.jpg)

En de joystick kan ingelezen worden;

![Joystick]({{ site.baseurl }}/assets/knob.jpg)

Als bonus: De touchscreen en joystick kunnen ook samenwerken:

<iframe width="360" height="640" src="{{ site.baseurl }}/assets/joystick_screen.webm" frameborder="0" allowfullscreen></iframe>
