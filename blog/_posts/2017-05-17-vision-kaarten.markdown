---
layout: post
cover: cover.jpg
title:  "Tracken kaartsymbolen"
date:   2017-05-17 10:40:27 +0200
---

De kaartsymbolen kunnen door de camera getrackt worden.

<iframe width="640" height="360" src="{{ site.baseurl }}/assets/symbolen.webm" frameborder="0" allowfullscreen></iframe>
