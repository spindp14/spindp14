---
cover: cover.jpg
layout: post
title:  "Spidercontrol"
date:   2017-06-20 13:28:27 +0200
---

Gisteren hebben we de spin bestuurd via de controller! Na het switchen van de 
arduino naar de raspberry (voor de controller) lukte het om via een verbinding
met bluetooth de spin te besturen. Zoals hieronder te zien is zijn wij zeer 
tevreden met deze vooruitgang.

<iframe width="640" height="360" src="{{ site.baseurl }}/assets/remote_walking.mp4" frameborder="0" allowfullscreen></iframe>
