import logging
import signal
import sys

from PyQt5 import Qt

from controllerpy import gui
from controllerpy import bt


def main():
    logging.basicConfig(level=logging.DEBUG)
    app = Qt.QApplication(sys.argv)
    app.setApplicationName('controller')

    signal.signal(signal.SIGINT, lambda *a: app.exit(-2))

    bluetooth = bt.Bluetooth(addr='B8:27:EB:B1:4B:63')

    form = gui.MainWindow(bluetooth)
    form.show()
    form.start()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
