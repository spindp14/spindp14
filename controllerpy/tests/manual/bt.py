from controllerpy import bt
from controllerpy import xyrocker

import logging
import random
import threading
import time


def write_coords(b):
    while True:
        coords = xyrocker.get_positions()
        b.write_coords(*coords)
        if random.uniform(0, 1) > 0.99:
            b.write_mode(1)
        time.sleep(0.01)


def main():
    logging.basicConfig(level=logging.DEBUG)
    b = bt.Bluetooth(addr='B8:27:EB:1F:59:59')
    # This thread represents any external thread that wants to write to the
    # bluetooth object.
    write_thread = threading.Thread(target=write_coords, args=(b,))
    write_thread.setDaemon(True)
    write_thread.start()
    try:
        b.loop()
    finally:
        b.disconnect()


if __name__ == '__main__':
    main()
