from setuptools import setup


requirements = [
    # Add requirements here.
]

test_requirements = [
    'pytest',
]

if __name__ == '__main__':
    setup(
        name='controllerpy',
        version='0.1.0',
        description='TODO',
        author='SpInDP 14',
        url='https://gitlab.com/spindp14/spindp14',
        packages=[
            'controllerpy',
        ],
        package_dir={
            'controllerpy': 'controllerpy'
        },
        include_package_data=True,
        install_requires=requirements,
        zip_safe=False,
        classifiers=[
            # TODO
            'Development Status :: 2 - Pre-Alpha',
        ],
        test_suite='tests',
        setup_requires=['pytest-runner'],
        tests_require=test_requirements,
    )
