import logging
import threading
import random
import sys

from PyQt5 import Qt

from . import MoveMode
from . import xyrocker

logger = logging.getLogger(__name__)


class BigButton(Qt.QPushButton):

    def __init__(self, *args, **kwargs):
        super(BigButton, self).__init__(*args, **kwargs)
        self.setCheckable(True)
        self.setMinimumHeight(50)


class ModeSelectWidget(Qt.QGroupBox):

    buttonChecked = Qt.pyqtSignal([int])

    def __init__(self, *args, **kwargs):
        super(ModeSelectWidget, self).__init__(*args, **kwargs)

        self._remoteBtn = BigButton(self.tr('Remote'), self)
        self._remoteBtn.setChecked(True)
        self._danceBtn = BigButton(self.tr('Dance'), self)
        self._eggBtn = BigButton(self.tr('Egg'), self)
        self._furyBtn = BigButton(self.tr('Fury'), self)

        self._group = Qt.QButtonGroup(self)
        self._group.addButton(self._remoteBtn, 1)
        self._group.addButton(self._danceBtn, 2)
        self._group.addButton(self._eggBtn, 3)
        self._group.addButton(self._furyBtn, 4)

        layout = Qt.QVBoxLayout(self)
        layout.addWidget(self._remoteBtn)
        layout.addWidget(self._danceBtn)
        layout.addWidget(self._eggBtn)
        layout.addWidget(self._furyBtn)

        self.setTabOrder(self._remoteBtn, self._danceBtn)
        self.setTabOrder(self._danceBtn, self._eggBtn)
        self.setTabOrder(self._eggBtn, self._furyBtn)

        self.setFlat(True)

        self._group.buttonToggled[int, bool].connect(self._emitButtonChecked)

    @Qt.pyqtSlot(int, bool)
    def _emitButtonChecked(self, id, checked):
        if checked:
            self.buttonChecked.emit(id)


class CoordsWidget(Qt.QWidget):

    changed = Qt.pyqtSignal([int, int])

    def __init__(self, *args, **kwargs):
        super(CoordsWidget, self).__init__(*args, **kwargs)

        self._text = 'x: {x:4} y: {y:4}'

        self._font = Qt.QFont('monospace')
        self._label = Qt.QLabel(self._text, self)
        self._label.setFont(self._font)
        self._timer = Qt.QTimer(self)

        layout = Qt.QHBoxLayout(self)
        layout.addWidget(self._label)

        self._timer.timeout.connect(self._updateCoords)

    def start(self, ms=50):
        self._timer.start(ms)

    @Qt.pyqtSlot()
    def _updateCoords(self):
        x, y = xyrocker.get_positions()
        # x, y = random.randint(0, 2043), random.randint(0, 2043)
        self._label.setText(self._text.format(x=x, y=y))
        self.changed.emit(x, y)


class IndicatorWidget(Qt.QLabel):

    changed = Qt.pyqtSignal([int])

    def __init__(self, *args, **kwargs):
        super(IndicatorWidget, self).__init__(*args, **kwargs)
        # Remember whether the button was pushed in the previous "iteration".
        # I have no better check.  If there were time, I'd do this better.
        self._previousPushed = False
        self._timer = Qt.QTimer(self)

        self._timer.timeout.connect(self._monitorChanged)
        self._status = MoveMode.WALK
        self._setStatusText(self._status)

    def start(self, ms=50):
        self._timer.start(ms)

    def _setStatusText(self, status):
        if status == MoveMode.WALK:
            self.setText(self.tr('Walk'))
        else:
            self.setText(self.tr('Turn'))

    @Qt.pyqtSlot()
    def _monitorChanged(self):
        result = xyrocker.get_button_state()
        if result and not self._previousPushed:
            logger.debug('button pressed')
            self._status = (MoveMode.WALK if self._status == MoveMode.TURN else
                            MoveMode.TURN)
            self._previousPushed = True
            self._setStatusText(self._status)
            self.changed.emit(self._status)
        if not result and self._previousPushed:
            logger.debug('button released')
            self._previousPushed = False


class DistanceWidget(Qt.QWidget):

    stepChanged = Qt.pyqtSignal([int])
    heightChanged = Qt.pyqtSignal([int])
    widthChanged = Qt.pyqtSignal([int])

    def __init__(self, *args, **kwargs):
        super(DistanceWidget, self).__init__(*args, **kwargs)

        self._stepLbl = Qt.QLabel(self.tr('Step'), self)
        self._stepLbl.setAlignment(Qt.Qt.AlignCenter)
        self._stepSlider = Qt.QDial(self)
        self._stepSlider.setMaximum(100)
        self._stepSlider.setValue(100)
        self._stepSlider.setNotchesVisible(True)
        self._heightLbl = Qt.QLabel(self.tr('Height'), self)
        self._heightLbl.setAlignment(Qt.Qt.AlignCenter)
        self._heightSlider = Qt.QDial(self)
        self._heightSlider.setMaximum(100)
        self._heightSlider.setValue(100)
        self._heightSlider.setNotchesVisible(True)
        self._widthLbl = Qt.QLabel(self.tr('Width'), self)
        self._widthLbl.setAlignment(Qt.Qt.AlignCenter)
        self._widthSlider = Qt.QDial(self)
        self._widthSlider.setMaximum(100)
        self._widthSlider.setValue(66)
        self._widthSlider.setNotchesVisible(True)

        stepLayout = Qt.QVBoxLayout()
        stepLayout.addWidget(self._stepLbl)
        stepLayout.addWidget(self._stepSlider)
        heightLayout = Qt.QVBoxLayout()
        heightLayout.addWidget(self._heightLbl)
        heightLayout.addWidget(self._heightSlider)
        widthLayout = Qt.QVBoxLayout()
        widthLayout.addWidget(self._widthLbl)
        widthLayout.addWidget(self._widthSlider)

        layout_ = Qt.QHBoxLayout()
        layout_.addLayout(stepLayout)
        layout_.addLayout(heightLayout)
        layout_.addLayout(widthLayout)

        layout = Qt.QVBoxLayout(self)
        layout.addStretch()
        layout.addLayout(layout_)
        layout.addStretch()

        self._stepSlider.valueChanged.connect(self._emitStepChanged)
        self._heightSlider.valueChanged.connect(self._emitHeightChanged)
        self._widthSlider.valueChanged.connect(self._emitWidthChanged)

    @Qt.pyqtSlot(int)
    def _emitStepChanged(self, value):
        self.stepChanged.emit(value)

    @Qt.pyqtSlot(int)
    def _emitHeightChanged(self, value):
        self.heightChanged.emit(value)

    @Qt.pyqtSlot(int)
    def _emitWidthChanged(self, value):
        self.widthChanged.emit(value)

    @property
    def step(self):
        return self._stepSlider.value()

    @property
    def height(self):
        return self._heightSlider.value()

    @property
    def width(self):
        return self._widthSlider.value()


class MainWidget(Qt.QWidget):

    def __init__(self, bluetooth, *args, **kwargs):
        super(MainWidget, self).__init__(*args, **kwargs)
        self._bluetooth = bluetooth
        self._bluetooth_thread = None

        self._modeSelect = ModeSelectWidget(self)
        self._distance = DistanceWidget(self)
        self._beakBtn = BigButton(self.tr('Beak'), self)
        self._beakBtn.setChecked(True)
        self._coords = CoordsWidget(self)
        self._indicator = IndicatorWidget(self)

        h_layout = Qt.QHBoxLayout()
        h_layout.addWidget(self._modeSelect)
        h_layout.addWidget(self._distance)

        coords_layout = Qt.QHBoxLayout()
        coords_layout.addStretch()
        coords_layout.addWidget(self._beakBtn)
        coords_layout.addWidget(self._indicator)
        coords_layout.addWidget(self._coords)

        v_layout = Qt.QVBoxLayout(self)
        v_layout.addLayout(h_layout)
        v_layout.addStretch()
        v_layout.addLayout(coords_layout)

        self._modeSelect.buttonChecked.connect(self._changeMode)
        self._distance.stepChanged.connect(self._changeDistance)
        self._distance.heightChanged.connect(self._changeDistance)
        self._distance.widthChanged.connect(self._changeDistance)
        self._beakBtn.toggled.connect(self._changeBeak)
        self._coords.changed.connect(self._changeCoords)
        self._indicator.changed.connect(self._changeMoveMode)

    def start(self):
        self._coords.start()
        self._indicator.start()
        self._bluetooth_thread = threading.Thread(target=self._bluetooth.loop)
        self._bluetooth_thread.start()

    @Qt.pyqtSlot(int)
    def _changeMode(self, id):
        self._bluetooth.write_mode(id)

    @Qt.pyqtSlot()
    def _changeDistance(self):
        self._bluetooth.write_distance(self._distance.step,
                                       self._distance.height,
                                       self._distance.width)

    @Qt.pyqtSlot(int, int)
    def _changeCoords(self, x, y):
        self._bluetooth.write_coords(x, y)

    @Qt.pyqtSlot(int)
    def _changeMoveMode(self, value):
        self._bluetooth.write_move_mode(value)

    @Qt.pyqtSlot(bool)
    def _changeBeak(self, value):
        self._bluetooth.write_beak(value)

    def __del__(self):
        self._bluetooth.stop()
        self._bluetooth_thread.join()


class MainWindow(Qt.QMainWindow):

    def __init__(self, bluetooth, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self._centralWidget = MainWidget(bluetooth, self)
        self.setCentralWidget(self._centralWidget)
        self.setFixedSize(480, 320)
        # self.showFullScreen()

    def start(self):
        self._centralWidget.start()
