import logging
import threading
import time
import json

import bluetooth

logger = logging.getLogger(__name__)

# For documentation, see spiderpy/bluetooth.  The files are very similar, but
# not similar enough to warrant subclassing, which is annoying.  I'd rather
# document things in one place than in two places, so yeah...


class Bluetooth(object):

    def __init__(self, uuid='16f07440-50fb-11e7-9598-0800200c9a66', addr=None):
        self._uuid = uuid
        self._addr = addr
        self.port_lock = threading.Lock()
        self.buffer_lock = threading.Lock()
        self.connected = False
        self._timeout = 0.05
        self._coords = (0, 0)
        self._buffer = []
        self._sock = None
        self._running = False

    def loop(self):
        self._running = True
        while self._running:
            if self.connected:
                try:
                    self._write_stuff()
                except bluetooth.btcommon.BluetoothError:
                    logger.debug('write failed', exc_info=1)
                    self.connected = False
                time.sleep(self._timeout)
            else:
                try:
                    self.connect()
                except bluetooth.btcommon.BluetoothError:
                    logger.debug('could not connect', exc_info=1)

    def _write_stuff(self):
        with self.buffer_lock:
            items = list(self._buffer)
            self._buffer[:] = []  # clear buffer
            items.append(self._create_coords_json(*self._coords))
        with self.port_lock:
            for item in items:
                self._sock.send(item + '\n')
                logger.debug(repr(item))

    def _create_coords_json(self, x, y):
        result = {'type': 'coords', 'x': x, 'y': y}
        return json.dumps(result)

    def write_coords(self, x, y):
        with self.buffer_lock:
            self._coords = (x, y)

    def write_mode(self, mode):
        mode_json = json.dumps({'type': 'mode', 'mode': mode})
        self._write_repeat(mode_json)

    def write_move_mode(self, move_mode):
        move_mode_json = json.dumps({'type': 'move_mode', 'move_mode': move_mode})
        self._write_repeat(move_mode_json)

    def write_distance(self, step, height, width):
        distance_json = json.dumps({'type': 'distance', 'step': step,
                                    'height': height, 'width': width})
        self._write_repeat(distance_json)

    def write_beak(self, toggled):
        beak_json = json.dumps({'type': 'beak', 'toggled': toggled})
        self._write_repeat(beak_json)

    def _write_repeat(self, value, n=5):
        """write *value* a few times to the buffer. if one of them doesn't go
        through, likely the next will.
        """
        with self.buffer_lock:
            for i in range(n):
                self._buffer.append(value)

    def connect(self):
        self.disconnect()
        logger.info('looking for server')
        matches = bluetooth.find_service(uuid=self._uuid, address=self._addr)
        if len(matches) == 0:
            raise bluetooth.btcommon.BluetoothError('no matches found')
        else:
            first = matches[0]
            port = first['port']
            name = first['name']
            host = first['host']

            logger.info('connecting to {} on {}'.format(name, host))

            self._sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
            self._sock.connect((host, port))
            self.connected = True

    def disconnect(self):
        try:
            self._sock.close()
        except:
            logger.debug("can't close socket", exc_info=1)

    def stop(self):
        self._running = False

    def __del__(self):
        self.disconnect()
