# Distributed with a free-will license.
# Use it any way you want, profit or free, provided it fits in the licenses of its associated works.
# MCP3428
# This code is designed to work with the MCP3428_I2CADC I2C Mini Module available from ControlEverything.com.
# https://www.controleverything.com/content/Analog-Digital-Converters?sku=MCP3428_I2CADC#tabs-0-product_tabset-2

import smbus
import RPi.GPIO as GPIO
from time import sleep

# Get I2C bus
I2C_ROCKER_ADRESS = 0x68
X_DIRECTION = 0x10
Y_DIRECTION = 0x30
bus = smbus.SMBus(1)
# GPIO setup on GPIO 5
GPIO.setmode(GPIO.BCM)
GPIO.setup(5, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def get_input(direction):
    # Send configuration command
    bus.write_byte(I2C_ROCKER_ADRESS, direction)
    sleep(0.005)

    # Read data back from 0x00(0), 2 bytes
    # raw_adc MSB, raw_adc LSB
    data = bus.read_i2c_block_data(I2C_ROCKER_ADRESS, 2)
    sleep(0.005)

    # Convert the data to 12-bits
    raw_adc = (data[0] & 0x0F) * 256 + data[1]
    if raw_adc > 2047:
        raw_adc -= 4095

    return raw_adc

def get_positions():
    x = get_input(X_DIRECTION)
    y = get_input(Y_DIRECTION)
    return (x, y)

def get_button_state():
    return not GPIO.input(5)
