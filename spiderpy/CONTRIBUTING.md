# Contributing

## Set up dev environment (Raspbian Jessie)

De volgende stappen zijn nodig voor een werkende dev env.  Voer deze commando's
uit:

    sudo apt-get update
    # All sorts of dependencies, vaguely combined in logical groups
    sudo apt-get -y install \
        python-opencv python-numpy python-picamera \
        python-pytest python-pip python-flake8 python-flask python-dev \
        python-mock python-yaml \
        python-bluez \
        build-essential \
        python-smbus python-serial i2c-tools \
        git tmux vim htop \
        python-enum python-enum34 python-pexpect
    
    # Clone and install spiderpy
    cd ~
    git clone https://gitlab.com/spindp14/spindp14.git
    cd spindp14/spiderpy
    pip install --user -e .
    cd ../controllerpy
    pip install --user -e .
    
    # Configure some Pi stuff
    sudo raspi-config nonint do_camera 0
    sudo raspi-config nonint do_i2c 0
    sudo raspi-config nonint do_serial 0
    sudo sed -i '/dtparam/s/^#//g' /boot/config.txt
    
    # Bluetooth in compat mode.  Add serial port profile
    sudo mkdir -p /etc/systemd/system/bluetooth.service.d
    sudo sh -c "cat << EOF > /etc/systemd/system/bluetooth.service.d/override.conf
    [Service]
    ExecStart=
    ExecStart=/usr/lib/bluetooth/bluetoothd -C
    ExecStartPost=/usr/bin/sdptool add --channel=22 SP
    EOF"

    # Bluetooth permissions
    sudo usermod -a -G bluetooth pi
    sudo sh -c "cat << EOF > /etc/systemd/system/run-sdp.path
    [Unit]
    Descrption=Monitor /run/sdp

    [Install]
    WantedBy=bluetooth.service

    [Path]
    PathExists=/run/sdp
    Unit=run-sdp.service
    EOF"

    sudo sh -c "cat << EOF > /etc/systemd/system/run-sdp.service
    [Unit]
    Description=Set permission of /run/sdp

    [Install]
    RequiredBy=run-sdp.path

    [Service]
    Type=simple
    ExecStart=/bin/chgrp bluetooth /run/sdp
    EOF"

    sudo systemctl daemon-reload
    sudo systemctl enable run-sdp.path
    sudo systemctl enable run-sdp.service

    # Open Bluetooth TTY port
    sudo sh -c "cat << EOF > /etc/systemd/system/rfcomm-watch.service
    [Unit]
    Description=Wait for connection
    Requires=bluetooth.service

    [Service]
    Type=simple
    ExecStart=/usr/bin/rfcomm -r watch 1 22 /sbin/agetty -L rfcomm1 115200

    [Install]
    WantedBy=multi-user.target
    EOF"
    sudo systemctl daemon-reload
    sudo systemctl enable rfcomm-watch.service

Bluetooth moet handmatig ingesteld worden met bluetoothctl (discoverable on,
scan on, pair, trust).

Om met de spin te verbinden:

    sudo rfcomm bind 0 ADDRESS 22
    minicom -s
    
Op de spin Pi:

    # Autologin
    sudo raspi-config nonint do_boot_behaviour B2
    sudo apt-get -y install libpam-systemd
    mkdir -p ~/.local/share/systemd/user
    cat << EOF > ~/.local/share/systemd/user/main.service
    [Unit]
    Description=Controller

    [Service]
    Type=simple
    ExecStart=/usr/bin/python '/home/pi/spindp14/spiderpy/tests/manual/main.py'

    [Install]
    WantedBy=default.target
    EOF
    systemctl --user daemon-reload
    systemctl --user enable main.service

Op de client Pi:

    sudo apt-get -y install python-pyqt5 xserver-xorg xserver-xorg-legacy \
        xinit lightdm
    sudo echo 'allowed_users=anybody' > /etc/X11/Xwrapper.config
    # Autologin with desktop
    sudo raspi-config nonint do_boot_behaviour B4
    echo 'export DISPLAY=:0' >> .profile
    sudo sed -i 's/#xserver-command=X/xserver-command=X -nocursor/' /etc/lightdm/lightdm.conf
    mkdir -p ~/.local/share/systemd/user
    cat << EOF > ~/.local/share/systemd/user/main.service
    [Unit]
    Description=Controller

    [Service]
    Type=simple
    Environment='DISPLAY=:0'
    ExecStart=/usr/bin/python '/home/pi/spindp14/controllerpy/tests/manual/main.py'

    [Install]
    WantedBy=default.target
    EOF
    systemctl --user daemon-reload
    systemctl --user enable main.service
    wget http://www.waveshare.com/w/upload/7/74/LCD-show-170309.tar.gz
    tar xvf LCD-show-*.tar.gz
    cd LCD-show
    ./LCD35-show  # This reboots the Pi!!!

Reboot aan het einde:

    sudo reboot

### References

https://raspberrypi.stackexchange.com/questions/47671/why-my-program-wont-communicate-through-ttyama0-on-raspbian-jessie

http://www.instructables.com/id/Raspberry-Pi-I2C-Python/

http://www.instructables.com/id/How-to-drive-Dynamixel-AX-12A-servos-with-a-Raspbe/
