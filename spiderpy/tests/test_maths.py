import itertools
import math

import pytest
import mock

from spiderpy import leg
from spiderpy import controller
from spiderpy import MAX_ANALOG_OUTPUT, MIDDLE_ANALOG_OUTPUT

TAU = math.pi * 2


def test_pythagoras():
    """Pythagoras is correctly calculated for lengths 1 and 2."""
    result = leg.pythagoras(1, 2)
    assert abs(result - 2.2) < 0.05


def test_cosine_law_legs():
    """cosine_law_legs is correctly calculated for lengths 5, 6 and 7."""
    values = [(5, -5), (6, -6), (7, -7)]

    for a, b, c in itertools.product(*values):
        result = leg.cosine_law_legs(a, b, c)
        assert abs(result - 1.4) < 0.05


def test_cosine_law_legs_2():
    """cosine_law_legs correctly handles domain problems."""
    values = [(6, -6), (3, -3), (2, -2)]

    for a, b, c in itertools.product(*values):
        result = leg.cosine_law_legs(a, b, c)
        assert result == 0


def test_cosine_law_SAS():
    """cosine_law_SAS is correctly calculated for lengths 5 and 6 and angle
    TAU/8.
    """
    values = [(5, -5), (6, -6), (TAU/8, -(TAU/8))]

    for a, b, gamma in itertools.product(*values):
        result = leg.cosine_law_SAS(a, b, gamma)
        assert abs(result - 4.3) < 0.05


def test_rotate_input():
    """The input rotation is correctly calculated"""
    legs = [leg.Leg((0, 1, 2), 0), leg.Leg((0, 1, 2), math.radians(90))]
    values = [(1, 0),
              (0, -1)]

    for spider_leg, (x, y) in zip(legs, values):
        result = spider_leg.rotate_input(x, y)
        assert abs(result[0] - 1) < 0.00000005
        assert abs(result[1] - 0) < 0.00000005


def test_inverse_kinematics():
    """The servo angles are correctly calculated"""
    spider_leg = leg.Leg((0, 1, 2), 0)

    result = spider_leg.inverse_kinematics(13, 0, 10, 0)
    assert abs(result[0]) == 0.0
    assert abs(result[1] - 0.04858) < 0.000005
    assert abs(result[2] - 1.51587) < 0.000005


#def


def test_calculate_destination():
    """The destination is correctly calculated"""
    l = leg.pythagoras(5, 10)
    a = -math.atan(0.5)

    spider_leg = leg.Leg((0, 1, 2), 0)
    result = spider_leg.calculate_destination(0,
                                              -1)
    print (result)
    assert abs(result[0] - l) < 0.00000005
    assert abs(result[1] - a) < 0.00000005


def test_calculate_input_angle():
    """The input angle is correctly calculated"""
    controller_ = controller.Controller(0)

    x_input = MAX_ANALOG_OUTPUT / 4
    y_input = MAX_ANALOG_OUTPUT / 4

    angle = controller_._calculate_input_angle(x_input, y_input)
    assert angle == TAU* 5/8


def test_calculate_destination_location():
    """The destination location is correctly calculated"""
    controller_ = controller.Controller(0)

    angle = TAU * 5/8
    res = -0.707

    locations = controller_._calculate_destination_location(angle)
    assert abs(locations[0] - res) < 0.0005
    assert abs(locations[0] - res) < 0.0005


def test_calculate_increment_location():
    """The incremented location is correctly calculated"""
    controller_ = controller.Controller(0)
    controller_.old_x = 0.5

    x2 = -1
    y2 = 0

    locations = controller_._calculate_increment_location(1, x2, y2)

    assert abs(locations[0] + 0.5) < 0.00000005
    assert locations[1] == 0
    assert locations[2] == False


#def test_calculate_incremented_destination():
    #"""The incremented destination is correctly calculated coming from the
    #middle position"""
    #l = leg.pythagoras(5, 10)
    #a = math.atan(0.5)

    #l_res = leg.pythagoras(2.5, 10)
    #a_res = math.atan(0.25)

    #spider_leg = leg.Leg((0, 1, 2), 0)
    #spider_leg.prev_length = 10
    #spider_leg.prev_angle = 0.0

    #result = spider_leg._calculate_incremented_destination(l, a, 2.5)
    #assert abs(result[0] - l_res) < 0.00000005
    #assert abs(result[1] - a_res) < 0.00000005
    #assert result[2] == False


#def test_calculate_incremented_destination_2():
    #"""The incremented destination is correctly calculated coming from the
    #opposite position"""
    #l = leg.pythagoras(5, 10)
    #a = math.atan(0.5)

    #spider_leg = leg.Leg((0, 1, 2), 0)
    #spider_leg.prev_length = l
    #spider_leg.prev_angle = -(a)

    #result = spider_leg._calculate_incremented_destination(l, a, 5)
    #assert abs(result[0] - 10) == 0
    #assert abs(result[1]) < 0.00000005
    #assert result[2] == False
