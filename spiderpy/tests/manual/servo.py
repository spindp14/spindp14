from spiderpy import ax12
from spiderpy import servo
import RPi.GPIO as GPIO

if __name__ == '__main__':
    leg2 = servo.Leg(2, 117.8448)
    leg3 = servo.Leg(3, 180.0)
    leg4 = servo.Leg(4, 242.1552)
    testAX12 = ax12.Ax12()

    leg2.inverse_kinematics(13, leg2.leg_height, 10, 0)
    leg3.inverse_kinematics(13, leg3.leg_height, 10, 0)
    leg4.inverse_kinematics(13, leg4.leg_height, 10, 0)
    #leg0.move(testAX12, start_pos)

    try:
        for i in range(1000)
            leg2.leg_walk(testAX12, 1024, 512)
            leg3.leg_walk(testAX12, 1024, 512)
            leg4.leg_walk(testAX12, 1024, 512)

    finally:
        GPIO.cleanup()
