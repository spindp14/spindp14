import logging

from spiderpy import bt


def main():
    logging.basicConfig(level=logging.DEBUG)
    b = bt.Bluetooth()
    try:
        b.loop()
    finally:
        b.disconnect()


if __name__ == '__main__':
    main()
