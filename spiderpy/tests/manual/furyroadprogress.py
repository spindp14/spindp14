import cv2
import numpy as np

from spiderpy import vision
from spiderpy import camera

""""Tests the color sensor by way of a simulated Fury Road
Point towards a black item, then periodically put a red item in front of it.
After the second red item, it should know to wait a bit.
"""

progress = 1
message = ""

if __name__ == '__main__':
    while True:
        frame_input = camera.capture()
        tiny_selection = vision.selection(frame_input, 10, 10)
        selection = cv2.cvtColor(tiny_selection,cv2.COLOR_BGR2HSV)
        if progress < 5:
            message, progress = vision.fury_road_progress(selection, progress)

        print(message)
        cv2.imshow("Camera", frame_input)
        cv2.imshow("Colorcheck", tiny_selection)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cv2.destroyAllWindows()