import logging
import signal

from spiderpy.controller import Controller
from spiderpy.spider import Spider

def main():
    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger('spiderpy.bt').setLevel(logging.INFO)
    bluetooth = None
    controller = Controller()
    spider = Spider(bluetooth, controller)
    signal.signal(signal.SIGINT, lambda *a: sys.exit(1))
    spider._hardcoded_fury()

if __name__ == '__main__':
    main()
