import logging
import signal
import threading

from spiderpy.bt import Bluetooth
from spiderpy.controller import Controller
from spiderpy.spider import Spider
from spiderpy import poc

def main():
    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger('spiderpy.bt').setLevel(logging.INFO)
    bluetooth = Bluetooth()
    controller = Controller()
    spider = Spider(bluetooth, controller)
    web_thread = threading.Thread(target=poc.run_webserver, args=(spider,))
    web_thread.setDaemon(True)
    web_thread.start()
    signal.signal(signal.SIGINT, lambda *a: spider.stop())
    spider.start()

if __name__ == '__main__':
    main()
