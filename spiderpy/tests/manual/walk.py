import time
import math
import random

from spiderpy import controller
import RPi.GPIO as GPIO

TAU = math.pi * 2


if __name__ == '__main__':
    controller = controller.Controller()
    try:
        controller.set_default(speed=50)
        time.sleep(4)
        for i in range(40):
            try:
                controller.walk(TAU/4, speed=400)
            except Exception as e:
                print(e)
                pass
            time.sleep(0.0001)
            #controller.chain.wait_stopped()

        time.sleep(1)
    finally:
        controller.chain.disable()
        controller.chain.port.close()
        GPIO.cleanup()
