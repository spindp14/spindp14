import cv2
import time

import numpy as np

from spiderpy import vision
from spiderpy import camera

def main():
    start = 0
    while True:
        frame = camera.capture()
        bgr = frame.copy()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        
        hearts, diamonds, spades, clubs = vision.all_matches(frame)
        cv2.drawContours(bgr, [hearts], -1, (0, 255, 0), 5)
        cv2.drawContours(bgr, [diamonds], -1, (255, 0, 0), 5)
        cv2.drawContours(bgr, [clubs], -1, (0, 0, 255), 5)
        cv2.drawContours(bgr, [spades], -1, (0, 255, 255), 5)

        cv2.imshow('bgr', bgr)
       

        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
            return

        end = time.clock()
        print(end - start)
        start = time.clock()


if __name__ == '__main__':
    main()
