import cv2
import numpy as np

from spiderpy import vision
from spiderpy import camera

""""Tests the pathfinding of the Fury Road system. Point the camera at a black
line, and the system will calculate the middle of the top and bottom segments,
and print these.
"""

if __name__ == '__main__':
    while True:
        frame_input = camera.capture()
        frame_hsv = cv2.cvtColor(frame_input, cv2.COLOR_BGR2HSV)
        print(vision.fury_road_path(frame_hsv))
        cv2.imshow('frame', frame_input)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cv2.destroyAllWindows()