import time
import math
import random
import logging

from spiderpy import controller
import RPi.GPIO as GPIO

TAU = math.pi * 2


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    controller = controller.Controller()
    try:
        try:
            controller.dance(speed=400)
        except Exception as e:
            logging.exception('ERROR')
    finally:
        controller.chain.disable()
        controller.chain.port.close()
        GPIO.cleanup()
