import cv2
import numpy as np

from spiderpy import vision
from spiderpy import camera

"""Tests the New And Improved(tm) symbol recognition. It takes the camera
feed, searches for rectangular contours, then tries to recognize symbols
inside them. If a symbol is found, the test will draw a small circle in the
middle of the rectangular contour:

Hearts = White
Diamond = Green
Clubs = Red
Spades = Blue
"""

def main():
    while True:
        frame = camera.capture()
        bgr = frame.copy()

        found_symbols = vision.find_symbols(frame)

        for symbol in found_symbols:
            if symbol[0] == 1:
                coord = symbol[1]
                cv2.circle(bgr, (coord[0],coord[1]), 5, (255,255,255))
            if symbol[0] == 2:
                coord = symbol[1]
                cv2.circle(bgr, (coord[0],coord[1]), 5, (0,255,0))
            if symbol[0] == 3:
                coord = symbol[1]
                cv2.circle(bgr, (coord[0],coord[1]), 5, (0,0,255))
            if symbol[0] == 4:
                coord = symbol[1]
                cv2.circle(bgr, (coord[0],coord[1]), 5, (255,0,0))

        cv2.imshow('bgr', bgr)
        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
            return

if __name__ == '__main__':
    main()
