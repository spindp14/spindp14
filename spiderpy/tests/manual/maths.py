import math

from mock import MagicMock

from spiderpy import leg


if __name__ == '__main__':
    leg0 = leg.Leg(0, math.radians(62.1552))

    x_input = 800
    y_input = 500

    ax12 = MagicMock()

    leg0.leg_walk(ax12, x_input, y_input)
