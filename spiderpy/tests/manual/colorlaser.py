import cv2
import numpy as np

from spiderpy import vision
from spiderpy import camera

# Make a small selection, and return the selection's average color upon
# quitting.

if __name__ == '__main__':
    while True:
        frame_input = camera.capture()
        selection = vision.selection(frame_input, 10, 10)

        average_color = np.average(np.average(selection, axis=0), axis=0)
        cv2.imshow('frame', frame_input)
        cv2.imshow('selection', selection)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            print(average_color)
            break

    cv2.destroyAllWindows()