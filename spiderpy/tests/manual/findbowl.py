import cv2
import numpy as np
from spiderpy import vision
from spiderpy import camera

if __name__ == '__main__':
    while True:
        frame_input = camera.capture()
        frame_gray = cv2.cvtColor(frame_input, cv2.COLOR_BGR2GRAY)
        cv2.imshow('frame', frame_input)
        cv2.imshow('gray', frame_gray)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    cv2.destroyAllWindows()