import cv2
import numpy as np

from spiderpy import vision
from spiderpy import camera

# Read video feed, find contours, return feed
# with contours drawn on it.

if __name__ == '__main__':
    while True:
        frame_input = camera.capture()
        frame = cv2.cvtColor(frame_input, cv2.COLOR_BGR2HSV)
        brown_contour = vision.find_egg(frame, vision.BROWN_EGG_MIN, vision.BROWN_EGG_MAX)
        white_contour = vision.find_egg(frame, vision.WHITE_EGG_MIN, vision.WHITE_EGG_MAX)

        if brown_contour is not None:
            cv2.drawContours(frame_input, [brown_contour], -1, (255, 0, 0), 5)
        if white_contour is not None:
            cv2.drawContours(frame_input, [white_contour], -1, (0, 255, 0),5)

        cv2.imshow('frame', frame_input)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cv2.destroyAllWindows()

