import threading
import time
import sys
import json
import logging

import bluetooth

from . import MoveMode

logger = logging.getLogger(__name__)


class Bluetooth(object):

    def __init__(self, uuid='16f07440-50fb-11e7-9598-0800200c9a66'):
        self._uuid = uuid
        # General lock to access buffered data.  I _could_ create specific
        # locks for specific members, but this will suffice.
        # (Buffer is probably the wrong word, but goodness me is naming stuff
        # difficult.)
        self.buffer_lock = threading.Lock()
        self.port_lock = threading.Lock()
        self.connected = False
        self._timeout = 0.5
        self._coords = (0, 0)
        self._move_mode = MoveMode.WALK
        self._server_sock = None
        self._client_sock = None
        self._client_info = None
        self._remainder = ''
        self._running = False
        self._step = 1
        self._height = 1
        self._width = 0.66
        self._mode = 1
        self._beak = True

    def loop(self):
        """Just loop the communication."""
        self._running = True
        while self._running:
            if self.connected:
                try:
                    self._consume()
                except bluetooth.btcommon.BluetoothError:
                    logger.debug('disconnected', exc_info=1)
                    self.connected = False
            # (Re-)connect.
            else:
                try:
                    self.connect()
                except bluetooth.btcommon.BluetoothError:
                    logger.debug('could not connect', exc_info=1)

    def _consume(self):
        """Read (consume) everything from remote and process it into instance
        variables.
        """
        with self.port_lock:
            # Read 1024 bytes OR LESS.
            data = self._client_sock.recv(1024)
        logger.debug(repr(data))

        lines = data.split('\n')
        lines[0] = '{}{}'.format(self._remainder, lines[0])
        self._remainder = lines.pop()
        for line in lines:
            try:
                self._process_line(line)
            except (ValueError, KeyError):
                logger.error('Could not read {!r}'.format(line))

    def _process_line(self, line):
        """Deserialise the line and do internal workings on it.  Assume it's
        valid json.  If not, raise a ValueError or (implicit) KeyError.
        """
        try:
            result = json.loads(line)
        except ValueError:
            raise
        type_ = result['type']
        if type_ == 'coords':
            self._process_coords(result)
        elif type_ == 'mode':
            self._process_mode(result)
        elif type_ == 'move_mode':
            self._process_move_mode(result)
        elif type_ == 'distance':
            self._process_distance(result)
        elif type_ == 'beak':
            self._process_beak(result)
        else:
            raise ValueError()

    def _process_coords(self, decoded):
        with self.buffer_lock:
            self._coords = (decoded['x'], decoded['y'])

    def _process_mode(self, decoded):
        with self.buffer_lock:
            self._mode = decoded['mode']

    def _process_move_mode(self, decoded):
        with self.buffer_lock:
            self._move_mode = decoded['move_mode']

    def _process_distance(self, decoded):
        with self.buffer_lock:
            self._step = decoded['step']
            self._height = decoded['height']
            self._width = decoded['width']

    def _process_beak(self, decoded):
        with self.buffer_lock:
            self._beak = decoded['toggled']

    def connect(self):
        """Create new socket and wait for connection."""
        self.disconnect()  # To be sure
        self._server_sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        self._server_sock.bind(('', bluetooth.PORT_ANY))
        self._server_sock.listen(1)
        self._port = self._server_sock.getsockname()[1]
        bluetooth.advertise_service(
            self._server_sock,
            "Spider server",
            service_id = self._uuid,
            service_classes = [ self._uuid, bluetooth.SERIAL_PORT_CLASS ],
            profiles = [ bluetooth.SERIAL_PORT_PROFILE ]
        )

        logger.info('waiting for connection')
        self._client_sock, self._client_info = self._server_sock.accept()
        logger.info('accepted connection from {}'.format(repr(self._client_info)))
        self.connected = True

    def disconnect(self):
        try:
            self._client_sock.close()
        # Bare except.  Not great, but I'm absolutely sure that I want to
        # capture _everything_.
        except:
            logger.debug("can't close client socket", exc_info=1)
        try:
            self._server_sock.close()
        except:
            logger.debug("can't close server socket", exc_info=1)

    def stop(self):
        self._running = False

    @property
    def coords(self):
        with self.buffer_lock:
            return self._coords

    @property
    def mode(self):
        with self.buffer_lock:
            return self._mode

    @property
    def move_mode(self):
        with self.buffer_lock:
            return self._move_mode

    @property
    def step(self):
        with self.buffer_lock:
            return self._step / 100.0

    @property
    def height(self):
        with self.buffer_lock:
            return self._height / 100.0

    @property
    def width(self):
        with self.buffer_lock:
            return self._width / 100.0

    @property
    def beak(self):
        with self.buffer_lock:
            return self._beak

    # There is no guarantee that this is run upon object deletion.  Always use
    # *disconnect* instead if possible.
    def __del__(self):
        self.disconnect()
