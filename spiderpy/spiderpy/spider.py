import threading
import math
import time
import logging
import sys

import cv2
from RPi import GPIO

from . import MoveMode
from . import gyro, leg, camera, vision, beak

TAU = math.pi * 2
XY_BUTTON_MAX = 2047.0
XY_BUTTON_MID = XY_BUTTON_MAX / 2

logger = logging.getLogger(__name__)


class Mode(object):
    REMOTE = 1
    FURY = 4


class FuryRoad(object):

    def __init__(self):
        self.progress = 1
        self.target_time = None
        self.start_wait = True

    def increment(self):
        self.progress += 1


class Spider(object):
    """The most important object that holds everything, basically."""

    def __init__(self, bluetooth, controller):
        self._bluetooth = bluetooth
        self._controller = controller

        self._bluetooth_thread = None
        self._running = False
        self._last_mode = Mode.REMOTE
        self._last_move_mode = MoveMode.WALK
        self.x_rot = 0
        self.y_rot = 0
        self._beak_up = True

    def start(self):
        """Start bluetooth connection and main loop."""
        self._running = True
        self._step_resize(1)
        self._height_resize(1)
        self._width_resize(2.0/3.0)
        self._bluetooth_thread = threading.Thread(target=self._bluetooth.loop)
        self._bluetooth_thread.setDaemon(True)
        try:
            self._controller.set_default(speed=50)
        except Exception:
            logger.exception('could not set default')
        beak.move_up()
        time.sleep(4)
        #gyro.setup()
        self._bluetooth_thread.start()
        logging.info('entering main loop')
        try:
            self._loop()
        except:
            logger.exception('_loop failed')
        finally:
            self.stop()

    def stop(self):
        """Stop bluetooth connection and main loop.  This is unfortunately
        rather buggy and cannot be relied upon.
        """
        print('STOP')
        self._running = False
        try:
            print('STOPPING CONTROLLER')
            self._controller.chain.disable()
            time.sleep(0.01)
            self._controller.chain.port.close()
            GPIO.cleanup()
        except:
            pass
        print('SHUT DOWN')
        sys.exit(1)

    def _loop(self):
        """Main loop."""
        while self._running:
            #coords = gyro.get_coords()
            #self.x_rot = gyro.get_x_rotation(*coords)
            #self.y_rot = gyro.get_y_rotation(*coords)
            if not self._bluetooth.connected:
                continue

            if self._bluetooth.mode != self._last_mode:
                if self._bluetooth.mode == Mode.FURY:
                    self._fury = FuryRoad()

            if self._bluetooth.mode == Mode.REMOTE:
                self._remote_mode()
                self._last_mode = Mode.REMOTE
            elif self._bluetooth.mode == Mode.FURY:
                self._hardcoded_fury()
                self._last_mode = Mode.FURY

    def _sleep_from_speed(self, speed):
        """Calculate the sleeps inbetween movements from the supplied speed."""
        return 0.0001

    def _swap_if_switched_move_mode(self, mode=None):
        if mode is None:
            mode = self._bluetooth.move_mode
        if self._last_move_mode != mode:
            try:
                self._controller.swap_legs_centered(speed=500)
            except:
                pass
            time.sleep(self._sleep_from_speed(500))
            self._last_move_mode = mode

    def _remote_mode(self):
        """Take the input from bluetooth and let the spider
        walk/turn accordingly.
        """
        x, y = self._bluetooth.coords
        x, y = min(x, XY_BUTTON_MAX - 1), min(y, XY_BUTTON_MAX - 1)
        self._resize(self._bluetooth.step, self._bluetooth.height,
                     self._bluetooth.width)
        speed = self._calculate_speed(x, y) * 1023
        if self._xy_is_centred(x, y):
            if self._bluetooth.beak and not self._beak_up:
                beak.move_up()
                self._beak_up = True
            elif not self._bluetooth.beak and self._beak_up:
                beak.move_down()
                self._beak_up = False
            return
        self._swap_if_switched_move_mode()
        if self._bluetooth.move_mode == MoveMode.WALK:
            try:
                self._controller.walk(self._calculate_input_angle(x, y),
                                    speed=speed)
            except:
                logger.debug('could not walk', exc_info=1)
            self._last_move_mode = MoveMode.WALK
            time.sleep(self._sleep_from_speed(speed))
        elif self._bluetooth.move_mode == MoveMode.TURN:
            speed = abs((x - XY_BUTTON_MID) / XY_BUTTON_MID) * 1023
            left = x < XY_BUTTON_MID
            try:
                self._controller.turn(left, speed=speed)
            except:
                logger.debug('could not turn', exc_info=1)
            self._last_move_mode = MoveMode.TURN
            time.sleep(self._sleep_from_speed(speed))

    def _fury_mode(self):
        """Takes the input from the camera and checks for both progress and
        pathfinding during the Fury Road Challenge.
        """
        max_difference = 15

        frame = cv2.cvtColor(camera.capture(), cv2.COLOR_BGR2HSV)
        if logger.level <= logging.DEBUG:
            cv2.imshow("frame", frame)
        if self._fury.progress > 5:
            pass
        else:
            self._fury.progress = vision.fury_road_progress(frame,
                                                            self._fury.progress
                                                            )
        (top, bottom, offset) = vision.fury_road_path(frame)

        if self._fury.progress == 5:
            if self._fury.start_wait:
                self._fury.start_wait = False
                self._fury.target_time = time.time() + 30
                self._controller.swap_legs_centered()
            else:
                if time.time() < self._fury.target_time:
                    time.sleep(1)
                else:
                    self._fury.increment()
            return

        if top - bottom > max_difference:
            logger.debug('turn right')
            self._swap_if_switched_move_mode(MoveMode.TURN)
            try:
                self._controller.turn(False, speed=100)
            except:
                logger.debug('could not turn', exc_info=1)
        elif (top - bottom) < -max_difference:
            logger.debug('turn left')
            self._swap_if_switched_move_mode(MoveMode.TURN)
            try:
                self._controller.turn(True, speed=100)
            except:
                logger.debug('could not turn', exc_info=1)
        # elif (offset - bottom) > max_difference:
        #     logger.debug('strafe right')
        #     self._swap_if_switched_move_mode(MoveMode.WALK)
        #     try:
        #         self._controller.walk(0, speed=100)
        #     except:
        #         logger.debug('could not strafe', exc_info=1)
        # elif (offset - bottom) < -max_difference:
        #     logger.debug('strafe left')
        #     print(offset, bottom, max_difference)
        #     self._swap_if_switched_move_mode(MoveMode.WALK)
        #     try:
        #         self._controller.walk(TAU/2, speed=100)
        #     except:
        #         logger.debug('could not strafe', exc_info=1)
        else:
            logger.debug('walk forward')
            self._swap_if_switched_move_mode(MoveMode.WALK)
            try:
                self._controller.walk(TAU/4, speed=100)
            except:
                logger.debug('could not walk', exc_info=1)

        cv2.waitKey(1)

    def _hardcoded_fury(self):
        target_time = time.time() + 30
        while target_time > time.time():
            logger.debug('walk forward')
            self._swap_if_switched_move_mode(MoveMode.WALK)
            try:
                self._controller.walk(TAU / 2, speed=100)
            except:
                logger.debug('could not walk', exc_info=1)
        time.sleep(2)
        target_time = time.time() + 30
        while target_time > time.time():
            logger.debug('walk forward')
            self._swap_if_switched_move_mode(MoveMode.WALK)
            try:
                self._controller.walk(TAU / 2, speed=100)
            except:
                logger.debug('could not walk', exc_info=1)
        self.controller.swap_legs_centered()

    def _xy_is_centred(self, x, y):
        """Return if the (x,y) input is close to the center."""
        return self._calculate_speed(x, y) < 0.1

    def _calculate_input_angle(self, x_input, y_input):
        """Calculate the walking direction using the (x,y) input."""
        x_input = float(x_input)
        y_input = float(y_input)

        x1 = (x_input - XY_BUTTON_MID) / XY_BUTTON_MID
        y1 = (y_input - XY_BUTTON_MID) / XY_BUTTON_MID

        if x1 > 0:
            angle = math.atan(y1/x1)
        elif x1 < 0:
            angle = TAU / 2 + math.atan(y1/x1)
        else:
            if y1 >= 0:
                angle = TAU / 4
            else:
                angle = TAU * 3 / 4
        return angle

    def _calculate_speed(self, x, y):
        """Calculate the speed from the pythagorean length to an (x,y)
        coordinate.
        """
        dx = (x - XY_BUTTON_MID) / XY_BUTTON_MID
        dy = (y - XY_BUTTON_MID) / XY_BUTTON_MID

        return leg.pythagoras(dx, dy)

    def _resize(self, step, height, width):
        self._step_resize(step)
        self._height_resize(height)
        self._width_resize(width)

    def _height_resize(self, size):
        """Resize the standing height of the spider.

        Size is a float in a range from 0 to 1.

        spider_height 9 - 13
        """
        size = max(0, size)
        size = min(1, size)
        self._controller.spider_height = 9 + size * 4

    def _step_resize(self, size):
        """Resize the step size of the spider.

        Size is a float in a range from 0 to 1.

        max_turn_angle TAU/64 - TAU/16
        max_step_radius 1 - 6
        step_height = 1 - 4.5
        """
        size = max(0, size)
        size = min(1, size)
        self._controller.max_turn_angle = (TAU / 64) + ((TAU * size) * (3 / 64.0))
        for leg in self._controller.legs:
            leg.max_step_radius = 1 + size * 5
            leg.step_height = 1 + size * 3.5

    def _width_resize(self, size):
        """Resize the width of the spider.

        Size is a float in a range from 0 to 1.

        middle_step_circle 2 - 14
        """
        size = max(0, size)
        size = min(1, size)
        for leg in self._controller.legs:
            leg.middle_step_circle = 2 + size * 12
