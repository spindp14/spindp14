from time import sleep
import math

from .servo import Servo
from . import MAX_ANALOG_OUTPUT, MIDDLE_ANALOG_OUTPUT

TAU = 2 * math.pi


def pythagoras(a, b):
    """Standard pythagoras calculation."""
    c = math.sqrt(math.pow(a, 2) + math.pow(b, 2))
    return c


def cosine_law_legs(a, b, c):
    """Calculate the angle of the corner opposite the middle given leg.

    a, b and c are the legs in a triangle.
    """
    if a == 0 or b == 0:
        return 0.25 * TAU
    if c == 0:
        return 0
    angle = ((math.pow(a, 2) + math.pow(b, 2) - math.pow(c, 2)) / abs(2 * a * b))

    if angle > 1:
        print "Max angle reached"
        angle = 1
    if angle < -1:
        print "Minimal angle reached"
        angle = -1

    gamma = math.acos(angle)
    return gamma


def cosine_law_SAS(a, b, gamma):
    """Calculate the leg opposite a given corner.

    a and b are legs in a triangle.
    gamma is a corner in a triangle.
    """
    gamma = abs(gamma)
    c = math.sqrt(math.pow(a, 2) + math.pow(b, 2)
                     - abs(2 * a * b * math.cos(gamma)))
    return c


class Leg(object):

    COXA = 2.275
    FEMUR = 6.35
    TIBIA = 14

    def __init__(self, servo_ids, body_angle):
        """servo_ids is tuple of three servo ids
        body_angle is the angle of the leg to the body in comparison to leg 0
        """
        # Consider removing leg ID.  It's not really needed or used
        self.id = servo_ids[0] / 3
        if self.id % 3 == 0:
            self.middle_to_shoulder = 7.9
        else:
            self.middle_to_shoulder = 11.514
        self.step_height = 4.5
        self.body_angle = body_angle
        self.max_step_radius = 6
        self.middle_step_circle = 10
        self.prev_length = 0.0
        self.prev_angle = 0.0
        self.servos = []
        for i, joint in zip(servo_ids, (Servo.TOP, Servo.MIDDLE,
                                        Servo.BOTTOM)):
            self.servos.append(Servo(i, joint))

    def servo_positions(self, top_angle, middle_angle, bottom_angle):
        """Calculate the servo positions using their corresponding angles."""
        positions = [
            MIDDLE_ANALOG_OUTPUT - (angle / TAU * MAX_ANALOG_OUTPUT)
            for angle in (top_angle, middle_angle, bottom_angle)
        ]

        return {servo.id: int(position) for servo, position in zip(self.servos,
                                                                   positions)}

    def current_height(self, phase):
        """Return the supposed height of a leg during the current phase."""
        if self.id % 2 == 0:
            if phase:
                return self.step_height
            else:
                return 0
        else:
            if phase:
                return 0
            else:
                return self.step_height

    #def calculate_positions(self, x_input, y_input):
        #length = pythagoras(x_input - MIDDLE_ANALOG_OUTPUT,
                            #y_input - MIDDLE_ANALOG_OUTPUT)

        ## Too small distance
        #if -20 < length < 20:
            #return

        #positions = self._rotate_input(x_input, y_input)
        #if self.phase == True:
            #destination = self._calculate_destination(positions[0],
                                                      #positions[1])
        #else:
            #destination = self._calculate_destination(MAX_ANALOG_OUTPUT -
                                                      #positions[0],
                                                      #MAX_ANALOG_OUTPUT -
                                                      #positions[1])

        #incremented_destination = self._calculate_incremented_destination(
            #destination[0], destination[1], destination[2])

        #if incremented_destination[2]:
            #self.phase = not self.phase

            #if self.phase == True:
                #self.leg_height = 2
            #else:
                #self.leg_height = 0

        #kinematics = self._inverse_kinematics(13.0, self.leg_height,
                                              #incremented_destination[0],
                                              #incremented_destination[1])

        #return self.servo_positions(*kinematics)

    def set_default(self, chain, spider_height, speed=10):
        """Place a leg into the default position."""
        positions = self.servo_positions(
            *self.inverse_kinematics(spider_height, 0, self.middle_step_circle, 0))
        for id, position in positions.items():
            servo = [servo for servo in self.servos if id == servo.id][0]
            servo.move(chain, position, speed=speed)

    def set_centered(self, chain, spider_height, phase, speed=10):
        """Place a leg into its centered position."""
        positions = self.servo_positions(
            *self.inverse_kinematics(spider_height, self.current_height(phase), self.middle_step_circle, 0))
        for id, position in positions.items():
            servo = [servo for servo in self.servos if id == servo.id][0]
            servo.move(chain, position, speed=speed)

    def walk(self, chain, positions, speed=10):
        """Move a leg to its designated servo positions."""
        for id, position in positions.items():
            servo = [servo for servo in self.servos if id == servo.id][0]
            servo.move(chain, position, speed)

    def rotate_input(self, x, y):
        """Rotate a point clockwise by the body angle around the middle analog
        output
        """
        angle = self.body_angle
        px, py = x, y
        ox, oy = 0, 0

        qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
        qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)

        return qx, qy


    def calculate_turn_destination(self, beta):
        """Calculate the destination of a turning leg with a given angle."""
        a = self.middle_to_shoulder
        c = self.middle_to_shoulder + self.middle_step_circle

        b = cosine_law_SAS(a, c, beta)
        gamma = cosine_law_legs(a, b, c)
        delta = TAU/2 - gamma
        if beta < 0:
            delta = -delta

        return (b, delta)


    def calculate_destination(self, x2, y2):
        """Calculate the angle of, and leg length to, the shoulder using
        coordinates in the step circle.
        """
        r2 = self.max_step_radius
        m = self.middle_step_circle

        x2 = float(x2) * r2
        y2 = float(y2) * r2

        k = m - x2

        l2 = pythagoras(y2, k)
        if not k == 0:
            delta2 = math.atan(y2 / k)
        else:
            if y2 >= 0:
                delta2 = TAU / 4
            else:
                delta2 = -TAU / 4

        return (l2, delta2)


    #def _calculate_incremented_destination(self, l2, delta2, d1):
        #l1 = self.prev_length
        #delta1 = self.prev_angle

        #l2 = float(l2)
        #delta2 = float(delta2)
        #d1 = float(d1 * 2)

        #d2 = cosine_law_SAS(l2, l1, abs(delta2 - delta1))

        ##Leg is at edge of step circle
        #if d1 > d2:
            #d1 = d2
            #return (l2 , delta2, True)

        #epsilon = cosine_law_legs(d2, l1, l2)

        #l3 = cosine_law_SAS(d1, l1, epsilon)

        #zeta = cosine_law_legs(l3, l1, d1)

        #if delta2 > delta1:
            #delta3 = zeta + delta1
        #else:
            #delta3 = -zeta + delta1

        #return (l3, delta3, False)

    def do_move(self, chain, length, angle, height, lift, speed=10):
        """Move a leg to a position using the angle of, and leg distance to, the
        shoulder."""
        kinematics = self.inverse_kinematics(height,
                                             lift,
                                             length,
                                             angle)

        positions = self.servo_positions(*kinematics)

        self.walk(chain, positions, speed=speed)

    def inverse_kinematics(self, h, g, l, angle):
        """Calculate the angles of the servos.

        h is the spider height.
        g is the leg height.
        l is the horizontal length of the leg to the shoulder.
        angle is the angle of the top servo.
        """
        f = self.COXA
        a = self.FEMUR
        c = self.TIBIA

        h = float(h)
        g = float(g)
        l = float(l)
        top_angle = float(angle)

        d = l - f
        e = h - g
        b = pythagoras(d, e)

        if e == 0:
            delta = (0.25 * TAU)
        else:
            delta = math.atan((d) / (e))

        gamma = cosine_law_legs(a, b, c)

        middle_angle = delta + gamma - (0.25 * TAU)

        beta = cosine_law_legs(a, c, b)

        bottom_angle = (0.5 * TAU) - beta

        self.prev_length = l
        self.prev_angle = angle

        return (top_angle, middle_angle, bottom_angle)
