from time import sleep
import math

TAU = 2 * math.pi


class Servo(object):

    TOP = 1
    MIDDLE = 2
    BOTTOM = 3

    def __init__(self, id, joint):
        """id is an integer for servo number.
        joint is one of three things: TOP, MIDDLE or BOTTOM
        """
        self.id = id
        self.joint = joint
        self.prev_position = None

    def move(self, chain, position, speed=10):
        self.prev_position = position
        chain.goto(self.id, position, blocking=False, speed=speed)
