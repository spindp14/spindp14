import RPi.GPIO as GPIO
from time import sleep

GPIO.setmode(GPIO.BCM)
GPIO.setup(26, GPIO.IN)

#checks if in 0.03 seconds the beat
#exists
def get_sound_input():
    sound_check = True
    for x in range (0,2):
        if GPIO.input(26) == 0:
           sound_check = False
           sleep(0.015)
           break
        sleep(0.015)
    return sound_check
