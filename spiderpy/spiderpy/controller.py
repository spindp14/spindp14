import itertools
import math
import time
import logging

#from spiderpy import gyro
from .dxl.dxlchain import DxlChain
from . import leg
from . import MAX_ANALOG_OUTPUT, MIDDLE_ANALOG_OUTPUT
from . import soundsensor

TAU = math.pi * 2

logger = logging.getLogger(__name__)


def wait_until(point_in_time):
    while True:
        now = time.time()
        if now < point_in_time:
            time.sleep(0.1)
        else:
            break


class Controller(object):

    def __init__(self, amount_legs=6):
        self.legs = []
        self.chain = DxlChain("/dev/serial0", rate=10**6)
        self.servo_list = self.chain.get_motor_list(first=1, last=18)
        self.phase = True
        self.old_x = 0
        self.old_y = 0
        self.turn_angle = 0
        self.max_turn_angle = TAU/16
        self._swap = False
        self.spider_height = 13

        #Show the list of servos to debug if one is missing

        # Check if all servos are accounted for
        if len(self.servo_list) < amount_legs * 3:
            raise RuntimeError('Not enough servos for {} legs. List: {}'.format(
                               amount_legs, self.servo_list))

        #Maybe change this later
        body_angles = [0.0, 62.1552, 117.8448, 180.0, 242.1552, 297.8448]
        for i, value in enumerate(body_angles):
            body_angles[i] = math.radians(value)

        # Split into evenly sized chunks of 3.
        for i, ids in enumerate(itertools.izip(*[iter(self.servo_list)] * 3)):
            self.legs.append(leg.Leg(ids, body_angles[i]))
        print(self.legs)

    def set_default(self, speed=10):
        """Set all legs in their default position."""
        try:
            for leg_ in self.legs:
               leg_.set_default(self.chain, self.spider_height, speed=speed)
        except:
            print "error default"

    def _incrementation_from_speed(self, speed):
        """Calculate the incrementation of leg movements using the supplied
        speed.
        """
        speed = min(speed, 1000)
        return max(0.1, speed / 1000.0)

    def turn(self, go_left, speed=10):
        """Turn the spider around its axis with a given speed."""
        if not self._swap:
            INCREMENTATION = self._incrementation_from_speed(speed)
            if not self.phase:
                go_left = not go_left

            if go_left:
                self.turn_angle = self.turn_angle - (self.max_turn_angle *
                                                     INCREMENTATION)
            else:
                self.turn_angle = self.turn_angle + (self.max_turn_angle *
                                                     INCREMENTATION)

            if abs(self.turn_angle) > self.max_turn_angle:
                modifier = 1 if self.turn_angle > 0 else -1
                self.turn_angle = self.max_turn_angle * modifier
                self._swap = True

            for leg_ in self.legs:
                if leg_.id % 2 == 0:
                    destination = leg_.calculate_turn_destination(self.turn_angle)
                else:
                    destination = leg_.calculate_turn_destination(-self.turn_angle)

                leg_.do_move(self.chain, destination[0], destination[1],
                             self.spider_height,
                             leg_.current_height(self.phase), speed=speed)

            if self._swap == True:
                self.phase = not self.phase

        else:
            self._swap_legs(speed=speed)

    def walk(self, angle, speed=10):
        """Walk in a specified direction with a given speed."""
        # Do regular movement
        if not self._swap:
            INCREMENTATION = self._incrementation_from_speed(speed)
            dest_locations = self._calculate_destination_location(angle)

            if self.phase:
                dest_x, dest_y = dest_locations
            else:
                dest_x = -dest_locations[0]
                dest_y = -dest_locations[1]

            inc_locations = self._calculate_increment_location(INCREMENTATION,
                                                          dest_x, dest_y)

            self.old_x = inc_locations[0]
            self.old_y = inc_locations[1]

            positions = dict()
            for leg_ in self.legs:
                # rotate the input for each leg
                if leg_.id % 2 == 0:
                    rot_locations = leg_.rotate_input(inc_locations[0],
                                                             inc_locations[1])
                else:
                    rot_locations = leg_.rotate_input(-inc_locations[0],
                                                             -inc_locations[1])

                destination = leg_.calculate_destination(*rot_locations)

                leg_.do_move(self.chain, destination[0], destination[1],
                             self.spider_height,
                             leg_.current_height(self.phase), speed=speed)

            # Are we at the end of the movement circle?
            if inc_locations[2]:
                self.phase = not self.phase
                self._swap = True
        # Lift/lower legs
        else:
            self._swap_legs(speed=speed)

    def _swap_legs(self, speed=10):
        """Lower and lift opposite legs."""
        speed = max(500, speed)
        self._swap = False
        self._place_legs(speed=speed)
        self._lift_legs(speed=speed)

    def swap_legs_centered(self, speed=10):
        """Center the lifted legs before swapping."""
        self._swap = False

        self._center_legs(speed=speed)

        self.phase = not self.phase

        self._place_legs(speed=speed)
        self._lift_legs(speed=speed)

        self._center_legs(speed=speed)

        self.old_x = 0
        self.old_y = 0
        self.turn_angle = 0

    def _center_legs(self, speed=10):
        """Center all lifted legs"""
        for leg_ in self.legs:
            if leg_.current_height(self.phase) == 0:
                continue

            leg_.set_centered(self.chain, self.spider_height, self.phase, speed=speed)

        # TODO: Fix this
        time.sleep(0.1)

    def _place_legs(self, speed=10):
        """Place down lifted legs."""
        for leg_ in self.legs:
            if leg_.current_height(self.phase) != 0:
                continue

            leg_.do_move(self.chain, leg_.prev_length, leg_.prev_angle,
                         self.spider_height,
                         leg_.current_height(self.phase), speed=speed)

        # TODO: Fix this
        time.sleep(0.1)

    def _lift_legs(self, speed=10):
        """Lift the standing legs."""
        for leg_ in self.legs:
            if leg_.current_height(self.phase) == 0:
                continue

            leg_.do_move(self.chain, leg_.prev_length, leg_.prev_angle,
                         self.spider_height,
                         leg_.current_height(self.phase), speed=speed)

        # TODO: Fix this
        time.sleep(0.1)

    def _calculate_destination_location(self, angle):
        """Extrapolate (x,y) coordinates from a given angle."""
        y2 = math.sin(angle)
        x2 = math.cos(angle)

        return (x2, y2)

    def _calculate_increment_location(self, d1, x2, y2):
        """Calculate the incremented leg coordinates using the current and new
        coordinates.

        d1 is the incrementation.
        (x2, y2) are the destination coordinates."""
        d2 = leg.pythagoras(x2 - self.old_x, y2 - self.old_y)
        if not d2 == 0:
            ratio = d1 / d2
        else:
            ratio = 1

        dx = ((1 - ratio) * self.old_x) + (ratio * x2)
        dy = ((1 - ratio) * self.old_y) + (ratio * y2)

        if leg.pythagoras(dx, dy) >= 1:
            return (x2, y2, True)

        return (dx, dy, False)

    def _nod(self, speed=10):
        intensity = speed / 100.0
        #Nod up.
        for leg_ in self.legs:
            if leg_.id == 1 or leg_.id == 2:
                leg_.set_default(self.chain,self.spider_height+intensity, speed=speed)
            elif leg_.id == 0 or leg_.id == 3:
                leg_.set_default(self.chain,self.spider_height+intensity/2, speed=speed)

        for leg_ in self.legs:
            leg_.set_default(self.chain,self.spider_height, speed=speed)

        #Nod down.
        for leg_ in self.legs:
            if leg_.id == 1 or leg_.id == 2:
                leg_.set_default(self.chain,self.spider_height-intensity, speed=speed)
            elif leg_.id == 0 or leg_.id == 3:
                leg_.set_default(self.chain,self.spider_height-intensity/2, speed=speed)

        for leg_ in self.legs:
            leg_.set_default(self.chain,self.spider_height, speed=speed)

    def _shake(self, speed=10):
        try:
            self.turn(True, speed=speed)
        except Exception:
            pass
        time.sleep(0.15)

        for leg_ in self.legs:
            try:
                leg_.set_centered(self.chain, self.spider_height, self.phase,
                                  speed=speed)
            except Exception:
                pass
        self.turn_angle = 0
        time.sleep(0.15)

        try:
            self.turn(False, speed=speed)
        except Exception:
            pass
        time.sleep(0.15)

        for leg_ in self.legs:
            try:
                leg_.set_centered(self.chain, self.spider_height, self.phase,
                                  speed=speed)
            except Exception:
                pass
        self.turn_angle = 0
        time.sleep(0.15)

    def _twist(self, speed=10):
        try:
            self.walk(TAU/2, speed=speed)
        except Exception:
            pass
        time.sleep(0.15)

        for leg_ in self.legs:
            try:
                leg_.set_centered(self.chain, self.spider_height, self.phase,
                                  speed=speed)
            except Exception:
                pass
        self.old_x = 0
        self.old_y = 0
        time.sleep(0.15)

        try:
            self.walk(0, speed=speed)
        except Exception:
            pass
        time.sleep(0.15)

        for leg_ in self.legs:
            try:
                leg_.set_centered(self.chain, self.spider_height, self.phase,
                                  speed=speed)
            except Exception:
                pass
        self.old_x = 0
        self.old_y = 0
        time.sleep(0.15)

    def _spin_around(self):
        for y in range (70):
            try:
                self.turn(True,300)
            except Exception:
                pass

    def _line_walk(self, speed):
        for y in range (0,20):
            try:
                self.walk(0, speed=speed)
            except Exception:
                pass
        for x in range (0,20):
            try:
                self.walk(TAU/2, speed=speed)
            except Exception:
                pass

    def dance(self, speed=10):
        start = time.time()

        self.set_default(speed=500)
        time.sleep(1)

        while start+14 > time.time():
            try:
                self.clap(speed=speed*0.5)
            except Exception:
                logger.exception('fail')

        wait_until(start + 15)
        while start + 38 > time.time():
            self._twist(speed=speed)
            self._shake(speed=speed)

        wait_until(start + 39)
        self.circle(speed=speed)
        try:
            self.swap_legs_centered(speed=speed)
        except Exception:
            pass
        self._spin_around()
        try:
            self.swap_legs_centered(speed=speed)
        except Exception:
            pass

        wait_until(start + 49)
        self._line_walk(speed=speed)
        while start + 60 > time.time():
            self._twist(speed=speed)
            self._shake(speed=speed)

        wait_until(start + 61)
        while start + 71 > time.time():
            self._twist(speed=speed*1.5)
            self._shake(speed=speed)

        wait_until(start + 78)
        self._shake(speed=speed)

        wait_until(start + 81)
        self._shake(speed=speed)

        wait_until(start + 84)
        while start + 87 > time.time():
            try:
                self._nod(speed=speed)
            except Exception:
                pass
            time.sleep(0.5)

        wait_until(start + 88)
        while start + 109 > time.time():
            self._twist(speed=speed)
            self._shake(speed=speed)

        wait_until(start + 110)
        while start + 119 > time.time():
            self._spin_around()
        try:
            self.swap_legs_centered(speed=speed)
        except Exception:
            pass
        


    def beat(self, speed=10):
        if soundcontroller.get_sound_input():
            self.clap(speed=speed)

    def clap(self, speed=10):
        for leg_ in self.legs:
            if leg_.id == 0:
                try:
                    #leg_.do_move(self.chain, 5, TAU/10,
                    #        self.spider_height + 4, leg_.step_height, speed=50)
                    pass
                except:
                    print "hello"
            elif leg_.id == 3:
                try:
                    #leg_.do_move(self.chain, 5, -TAU/10,
                    #        self.spider_height + 4, leg_.step_height, speed=50)
                    pass
                except:
                    print "3"
            if leg_.id == 1:
                try:
                    leg_.do_move(self.chain, leg_.prev_length, TAU/8,
                                 self.spider_height, leg_.step_height, speed=speed)
                except:
                    print "1"
            elif leg_.id == 2:
                try:
                    leg_.do_move(self.chain, leg_.prev_length, -TAU/8,
                                 self.spider_height, leg_.step_height, speed=speed)
                except:
                    print "2"
        time.sleep(0.35)

        for leg_ in self.legs:
            if leg_.id == 1:
                try:
                    leg_.do_move(self.chain, leg_.prev_length, 0,
                             self.spider_height, leg_.step_height, speed=speed)
                except:
                    print "1"
            elif leg_.id == 2:
                try:
                    leg_.do_move(self.chain, leg_.prev_length, 0,
                             self.spider_height, leg_.step_height, speed=speed)
                except:
                    print "2"
        time.sleep(0.35)

    def line_dance_setup(self, speed=10):
        self.phase = True
        self.set_default(speed=speed)
        for leg_ in self.legs:
            if leg_.id == 0:
                leg_.do_move(self.chain, leg_.prev_length, TAU/16,
                             self.spider_height, 0, speed=speed)
            elif leg_.id == 3:
                leg_.do_move(self.chain, leg_.prev_length, -TAU/16,
                             self.spider_height, 0, speed=speed)

    def circle(self, speed=10):
        angle = 0
        for x in range(50):
            angle += TAU / 32.0
            try:
                self.walk(angle, speed=speed)
            except:
                pass
        #self._spin_around()

        #for leg_ in self.legs:
        #    time.sleep(0.1)
        #    if leg_.id == 1:
        #        leg_.do_move(self.chain, leg_.prev_length, 0,
        #                     self.spider_height, leg_.step_height, speed=speed)
        #    elif leg_.id == 2:
        #        leg_.do_move(self.chain, leg_.prev_length, 0,
        #                     self.spider_height, leg_.step_height, speed=speed)
