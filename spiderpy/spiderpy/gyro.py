import math
import time

import smbus


# Power management registers
POWER_MGMT_1 = 0x6b
POWER_MGMT_2 = 0x6c

BUS = smbus.SMBus(1) # or BUS = smbus.SMBus(1) for Revision 2 boards
ADDRESS = 0x68       # This is the ADDRESS value read via the i2cdetect command


def read_byte(address):
    return BUS.read_byte_data(ADDRESS, address)


def read_word(address):
    high = BUS.read_byte_data(ADDRESS, address)
    low = BUS.read_byte_data(ADDRESS, address + 1)
    val = (high << 8) + low
    return val


def read_word_2c(address):
    val = read_word(address)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val

def get_coords():
        x = read_word_2c(0x3b)
        y = read_word_2c(0x3d)
        z = read_word_2c(0x3f)

        x_scaled = x / 16384.0
        y_scaled = y / 16384.0
        z_scaled = z / 16384.0

        return x_scaled, y_scaled, z_scaled

def distance(a, b):
    return math.sqrt((a * a) + ( b * b))


def get_y_rotation(x, y, z):
    radians = math.atan2(x, distance(y, z))
    return -math.degrees(radians)


def get_x_rotation(x, y, z):
    radians = math.atan2(y, distance(x, z))
    return math.degrees(radians)


def setup():
    BUS.write_byte_data(ADDRESS, POWER_MGMT_1, 0)
    time.sleep(0.1)


if __name__ == '__main__':

    # Now wake the 6050 up as it starts in sleep mode
    BUS.write_byte_data(ADDRESS, POWER_MGMT_1, 0)
    time.sleep(0.1)

    while True:
        #gyro_xout = read_word_2c(0x43)
        #gyro_yout = read_word_2c(0x45)
        #gyro_zout = read_word_2c(0x47)

        #print "gyro_xout : ", gyro_xout, " scaled: ", (gyro_xout / 131)
        #print "gyro_yout : ", gyro_yout, " scaled: ", (gyro_yout / 131)
        #print "gyro_zout : ", gyro_zout, " scaled: ", (gyro_zout / 131)

        accel_xout = read_word_2c(0x3b)
        accel_yout = read_word_2c(0x3d)
        accel_zout = read_word_2c(0x3f)

        accel_xout_scaled = accel_xout / 16384.0
        accel_yout_scaled = accel_yout / 16384.0
        accel_zout_scaled = accel_zout / 16384.0

        #print "accel_xout: ", accel_xout, " scaled: ", accel_xout_scaled
        #print "accel_yout: ", accel_yout, " scaled: ", accel_yout_scaled
        #print "accel_zout: ", accel_zout, " scaled: ", accel_zout_scaled

        print "x rotation: {}".format(get_x_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled))
        print "y rotation: {}".format(get_y_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled))
        print()
