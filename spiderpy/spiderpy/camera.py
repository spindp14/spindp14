from picamera.array import PiRGBArray
from picamera import PiCamera
from time import sleep
import cv2


camera = PiCamera(resolution=(640, 480), framerate=60)


def capture(camera=camera):
    with PiRGBArray(camera) as stream:
        camera.capture(stream, format='bgr', use_video_port=True)
        # At this point the image is available as stream.array
        image = stream.array
    return image


# Demo code. No need to actually run this.
if __name__ == '__main__':
    for frame in camera():
        cv2.imshow("Frame", frame)
        key = cv2.waitKey(1) & 0xFF
        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
            break
