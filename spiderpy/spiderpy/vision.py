from pkg_resources import resource_filename
import logging

import cv2

import numpy as np

logger = logging.getLogger(__name__)

def shape_contour(file_name):
    """Create a contour from an image file."""
    shape = cv2.imread(file_name)
    shape = cv2.cvtColor(shape, cv2.COLOR_BGR2GRAY)
    ret, shape = cv2.threshold(shape, 240, 255, cv2.THRESH_BINARY_INV)
    shape_contours, hierarchy = cv2.findContours(shape, cv2.RETR_TREE,
                                                 cv2.CHAIN_APPROX_SIMPLE)
    return shape_contours[0]


HEARTS = shape_contour(resource_filename('spiderpy', 'resources/hearts.png'))
CLUBS = shape_contour(resource_filename('spiderpy', 'resources/clubs.png'))
DIAMONDS = shape_contour(resource_filename('spiderpy', 'resources/diamonds.png'))
SPADES = shape_contour(resource_filename('spiderpy', 'resources/spades.png'))

# Thresholding values for brown and white eggs in HSV
BROWN_EGG_MIN = (5, 60, 25)
BROWN_EGG_MAX = (20, 255, 255)

WHITE_EGG_MIN = (0, 0, 150)
WHITE_EGG_MAX = (255, 75, 255)

CIRCLE_THRESHOLD = 0.5

WHITE_PAPER_MIN = np.array([0, 0, 0])
WHITE_PAPER_MAX = np.array([180, 255, 60])


WIDTH = 640
HEIGHT = 480


def red(hsv):
    """Filter red out from an hsv frame."""
    result = cv2.inRange(hsv, (0, 130, 60), (15, 255, 255))
    result = cv2.bitwise_or(result, cv2.inRange(hsv, (160, 130, 60),
                                                (179, 255, 255)))
    return cv2.GaussianBlur(result, (3, 3), 0)


def black(hsv):
    """Filter out black from an hsv frame."""
    return cv2.GaussianBlur(cv2.inRange(hsv, (0, 0, 0), (179, 255, 100)),
                            (3, 3), 0)


def best_match(contours, shape):
    """From a generator of contours, return the one that matches the given
    shape the best.
    """
    best = None
    lowest_number = 100  # Start with some big number really
    for cnt in contours:
        if cv2.contourArea(cnt) < 400:
            continue
        result = cv2.matchShapes(cnt, shape, cv2.cv.CV_CONTOURS_MATCH_I1, 0.0)
        if result < 0.08 and result < lowest_number:
            best = cnt

    return best


def best_match_result(contours, shape):
    """From a generator of contours, return the score of the best match
    """
    best_result = 0
    lowest_number = 100 #Arbitrarily high
    for cnt in contours:
        if cv2.contourArea(cnt) < 400:
            continue
        result = cv2.matchShapes(cnt, shape, cv2.cv.CV_CONTOURS_MATCH_I1, 0.0)
        if result < 0.08 and result < lowest_number:
            best_result = result

    return best_result


def all_matches(hsv):
    """Match all hearts, diamonds, spades and clubs in an hsv frame.

    Return a tuple of contours in that order.
    """
    red_img = red(hsv)
    cv2.imshow('red', red_img)
    black_img = black(hsv)
    cv2.imshow('black', black_img)

    red_contours, _ = cv2.findContours(red_img, cv2.RETR_TREE,
                                       cv2.CHAIN_APPROX_SIMPLE)
    hearts = best_match(red_contours, HEARTS)
    diamonds = best_match(red_contours, DIAMONDS)

    black_contours, _ = cv2.findContours(black_img, cv2.RETR_TREE,
                                         cv2.CHAIN_APPROX_SIMPLE)
    spades = best_match(black_contours, SPADES)
    clubs = best_match(black_contours, CLUBS)

    return hearts, diamonds, spades, clubs


def symbol_match(hsv):
    """Takes an HSV image and looks for a symbol match. It assumes there
    is only one symbol in the image.

    Return an integer that represents the found symbol:
    None: 0
    Hearts: 1
    Diamonds: 2
    Spades: 3
    Clubs: 4
    """
    red_img = red(hsv)
    black_img = black(hsv)

    red_symbol = True

    if np.average(black_img) > np.average(red_img):
        red_symbol = False

    symbol_checks = []

    if red_symbol:
        red_contours, _ = cv2.findContours(red_img, cv2.RETR_TREE,
                                           cv2.CHAIN_APPROX_SIMPLE)
        symbol_checks.append([best_match_result(red_contours, HEARTS),
                              1])
        symbol_checks.append([best_match_result(red_contours, DIAMONDS),
                              2])
    else:
        black_contours, _ = cv2.findContours(black_img, cv2.RETR_TREE,
                                             cv2.CHAIN_APPROX_SIMPLE)
        symbol_checks.append([best_match_result(black_contours, SPADES),
                              3])
        symbol_checks.append([best_match_result(black_contours, CLUBS),
                              4])

    result = 0
    lowest_number = 100
    for symbol in symbol_checks:
        if symbol[0] < lowest_number:
            lowest_number = symbol[0]
            result = symbol[1]

    return result


def centre_of_contour(contour):
    moments = cv2.moments(contour)
    x = int(moments['m10'] / moments['m00'])
    y = int(moments['m01'] / moments['m00'])

    return (x, y)


def offset_from_centre(frame, point):
    """Horizontal right of centre is positive.
    Vertical north of centre is positive.
    """
    shape = frame.shape
    x_offset = point[0] - shape[1] / 2
    y_offset = shape[0] / 2 - point[1]
    return (x_offset, y_offset)


def find_egg(frame_hsv, thres_min, thres_max):

    # Threshold and find contours
    threshold = cv2.inRange(frame_hsv, thres_min, thres_max)

    (cnts, _) = cv2.findContours(threshold.copy(), cv2.RETR_CCOMP,
                                 cv2.CHAIN_APPROX_SIMPLE)

    # Check all contours, compare, and find the one closest to a circle:
    # the one most likely to be an egg.

    lowest_difference = CIRCLE_THRESHOLD
    best_match = None

    for c in cnts:
        c_area = cv2.contourArea(c)
        if c_area > 200:

            (x, y), radius = cv2.minEnclosingCircle(c)
            radius = int(radius)

            circle_area = np.pi * np.square(radius)
            difference = (circle_area - c_area) / circle_area

            if difference < lowest_difference:
                lowest_difference = difference
                best_match = c

    return centre_of_contour(best_match)


def selection(frame, width, height, x_offset=0, y_offset=0):
    """Takes a selection from the frame and turns it into its own image.
    The initial starting pixel is the center of the screen, and the rectangle
    will be made with the starting pixel as the center of the ROI
    """
    center_x = WIDTH / 2
    center_y = HEIGHT / 2

    return frame[center_y - (height / 2) + y_offset:
                 center_y + (height / 2) + y_offset,
                 center_x - (width / 2) + x_offset:
                 center_x + (width / 2) + x_offset]


def order_points(pts):
    """Takes a list of 4 points and orders them for four-point transformation
    """
    rectangle = np.zeros((4, 2), dtype="float32")

    s = np.sum(pts, axis=1)
    rectangle[0] = pts[np.argmin(s)]
    rectangle[2] = pts[np.argmax(s)]

    diff = np.diff(pts, axis=1)
    rectangle[1] = pts[np.argmin(diff)]
    rectangle[3] = pts[np.argmax(diff)]

    return rectangle


def four_point_transform(image, pts):
    """Takes four points of an image and warps them into the corners of a
    rectangular selection image.
    """
    rectangle = order_points(pts)
    (tl, tr, br, bl) = rectangle

    width_a = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    width_b = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    max_width = max(int(width_a), int(width_b))

    height_a = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    height_b = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    max_height = max(int(height_a), int(height_b))

    dest = np.array([
        [0, 0],
        [max_width - 1, 0],
        [max_width - 1, max_height - 1],
        [0, max_height - 1]], dtype="float32")

    matrix = cv2.getPerspectiveTransform(rectangle, dest)
    warped = cv2.warpPerspective(image, matrix, (max_width, max_height))

    return warped


def average(frame):
    """Takes an image and returns an array containing average H, S and V"""
    return np.average(np.average(frame, axis=0), axis=0)


def fury_road_progress(frame_hsv, progress):
    """Checks the current progress on following the Fury Road
    by ROI-ing a certain area, checking its color, and comparing it to earlier
    progress
    """
    if progress % 2 == 1:
        if average(frame_hsv)[0] < 11:
            progress += 1
    else:
        if average(frame_hsv)[2] < 50:
            progress += 1

    return progress


def largest_contour_area(contours):
    """Takes a collection of contours and returns the one with the largest
    area.
    """
    largest_area = 0
    largest_cnt = None
    for cnt in contours:
        cnt_area = cv2.contourArea(cnt)
        if cnt_area > largest_area:
            largest_area = cnt_area
            largest_cnt = cnt
    return largest_cnt


def find_fury_road(frame_hsv):
    """Takes the largest black contour in the image and returns it.
    """
    frame_black = cv2.inRange(frame_hsv, (0, 0, 0), (180, 255, 70))
    if logger.level <= logging.DEBUG:
        cv2.imshow("thresh", frame_black)
    (cnts, _) = cv2.findContours(np.copy(frame_black), cv2.RETR_CCOMP,
                                 cv2.CHAIN_APPROX_SIMPLE)

    if len(cnts) is not None:
        return largest_contour_area(cnts)
    else:
        return None


def fury_road_path(frame_hsv):
    """Takes the largest black contour, and cuts off two ROIs: a topmost and a
    bottommost segment of the contour. Then, it takes the middle points of both
    contour segments, and returns these middle points, along with the offset of
    the bottom point from the center.
    """
    road_contour = find_fury_road(frame_hsv)
    segment_height = HEIGHT / 10
    try:
        ext_top = tuple(road_contour[road_contour[:, :, 1].argmin()][0])
        ext_bot = tuple(road_contour[road_contour[:, :, 1].argmax()][0])
    except cv2.error and TypeError:
        print("Could not find a proper contour")
        return -9999, -9999, -9999

    top_roi = frame_hsv[ext_top[1]:ext_top[1] + segment_height, 0:WIDTH]
    top_thres = cv2.inRange(top_roi, (0, 0, 0), (180, 255, 70))

    bot_roi = frame_hsv[ext_bot[1] - segment_height:ext_bot[1], 0:WIDTH]
    bot_thres = cv2.inRange(bot_roi, (0, 0, 0), (180, 255, 70))

    if logger.level <= logging.DEBUG:
        try:
            cv2.imshow("top_roi", top_roi)
            cv2.imshow("bot_roi", bot_roi)
        except Exception:
            pass
    try:
        top_center = path_segment_center(top_thres)[0]
        bot_center = path_segment_center(bot_thres)
        bot_offset = offset_from_centre(bot_roi, bot_center)[0]
    except cv2.error:
        print("Could not properly find a center for one of the segments")
        return -9999, -9999, -9999

    return top_center, bot_center[0], bot_offset


def path_segment_center(roi_thres):
    conts, _ = cv2.findContours(roi_thres, cv2.RETR_TREE,
                                   cv2.CHAIN_APPROX_SIMPLE)
    return centre_of_contour(largest_contour_area(conts))


def find_symbols(frame_bgr):
    gray = cv2.cvtColor(frame_bgr, cv2.COLOR_BGR2GRAY)
    gray_blur = cv2.GaussianBlur(gray, (3, 3), 0)
    edged = cv2.Canny(gray_blur, 10, 250)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)
    (cnts, _) = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL,
                                 cv2.CHAIN_APPROX_SIMPLE)

    # rectangles is a list of [paper contour, warped image made w contour]
    rectangles = []
    for c in cnts:
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)

        # pointslist is a list of the 4 corner coordinates
        pointslist = []
        if len(approx) == 4:
            cv2.drawContours(frame_bgr, [approx], -1, (0, 255, 0), 4)
            for element in approx:
                coord = element[0]
                cv2.circle(frame_bgr, (coord[0], coord[1]), 2, (0, 0, 255))
                pointslist.append((coord[0], coord[1]))
            transformed_rect = four_point_transform(frame_bgr, pointslist)
            rectangles.append([c, transformed_rect])

    # symbol_locations is a list of [symbol, center of paper contour]
    symbol_locations = []
    for rect in rectangles:
        rect_hsv = cv2.cvtColor(rect[1], cv2.COLOR_BGR2HSV)
        result = symbol_match(rect_hsv)
        if result == 0:
            continue
        contour = rect[0]
        (middle_x, middle_y) = centre_of_contour(contour)

        symbol_locations.append([result, [middle_x, middle_y]])

    return symbol_locations
