"""Proof of Concept for WebApp"""

import random
import time
from flask import Flask, jsonify, request, render_template


app = Flask(__name__)


def gyro():
    x = '{:.2f}'.format(app.state.x_rot)
    y = '{:.2f}'.format(app.state.y_rot)
    return (x, y)

def servo_positions():
    positions = []
    legs = app.state._controller.legs
    for leg in legs:
        leg_positions = []
        for servo in leg.servos:
            leg_positions.append((servo.id, servo.prev_position))
        positions.append(leg_positions)
    return '<br> '.join(map(str, positions))


@app.route('/_data')
def json_data():
    return jsonify(coords=app.state._bluetooth.coords,
                   servos=servo_positions(), gyro=gyro())


@app.route('/')
def index():
    return render_template('index.html')


def run_webserver(state):
    app.state = state
    app.run(host='0.0.0.0')
