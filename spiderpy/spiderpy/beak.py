#!/bin/python
from RPi import GPIO
from time import sleep

GPIO.setup(12, GPIO.OUT)
p = GPIO.PWM(12, 30)
p.start(6)

def move(angle):
    try:
        p.ChangeDutyCycle(angle)
        sleep(0.1)
    except:
        pass

def move_up():
    move(4)


def move_down():
    move(2.1)
