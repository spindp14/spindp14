#https://learn.adafruit.com/raspberry-pi-analog-to-digital-converters/mcp3008
#follow the steps
# Import SPI library (for hardware SPI) and MCP3008 library.
import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008

# Hardware SPI configuration:
SPI_PORT = 0
SPI_DEVICE = 0
MCP = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))

#min = 2 volt output adc
BATTERY_MIN = 635
#max = 3.3 volt output adc
BATTERY_MAX = 1023
BATTERY_DIFF = BATTERY_MAX - BATTERY_MIN


def battery_remaining():
    battery_current = MCP.read_adc(0)
    battery_now = battery_current - BATTERY_MIN
    battery_life = 100 * battery_now / BATTERY_DIFF
    return battery_life
